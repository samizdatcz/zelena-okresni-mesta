$container = $ ig.containers.list
$container.html ''
data =
  * nazev: "Benešov"
    poradi: 23
    procent: "24,57"
  * nazev: "Beroun"
    poradi: 10
    procent: "40,39"
  * nazev: "Blansko"
    poradi: 5
    procent: "55,89"
  * nazev: "Bruntál"
    poradi: 38
    procent: "16,18"
  * nazev: "Břeclav"
    poradi: 13
    procent: "37,66"
  * nazev: "Česká Lípa"
    poradi: 25
    procent: "23,87"
  * nazev: "Český Krumlov"
    poradi: 21
    procent: "26,69"
  * nazev: "Děčín"
    poradi: 1
    procent: "63,45"
  * nazev: "Domažlice"
    poradi: 41
    procent: "14,09"
  * nazev: "Frýdek-Místek"
    poradi: 20
    procent: "27"
  * nazev: "Havířov"
    poradi: 48
    procent: "10,32"
  * nazev: "Havlíčkův Brod"
    poradi: 44
    procent: "12,47"
  * nazev: "Hodonín"
    poradi: 4
    procent: "59,1"
  * nazev: "Cheb"
    poradi: 33
    procent: "18,98"
  * nazev: "Chomutov"
    poradi: 19
    procent: "27,4"
  * nazev: "Chrudim"
    poradi: 57
    procent: "3,97"
  * nazev: "Jablonec nad Nisou"
    poradi: 8
    procent: "41,34"
  * nazev: "Jeseník"
    poradi: 2
    procent: "60,46"
  * nazev: "Jičín"
    poradi: 55
    procent: "4,74"
  * nazev: "Jindřichův Hradec"
    poradi: 42
    procent: "13,14"
  * nazev: "Karviná"
    poradi: 35
    procent: "18,53"
  * nazev: "Kladno"
    poradi: 12
    procent: "38,22"
  * nazev: "Klatovy"
    poradi: 30
    procent: "19,71"
  * nazev: "Kolín"
    poradi: 51
    procent: "8,61"
  * nazev: "Kroměříž"
    poradi: 37
    procent: "16,39"
  * nazev: "Kutná Hora"
    poradi: 54
    procent: "6,71"
  * nazev: "Litoměřice"
    poradi: 58
    procent: "3,34"
  * nazev: "Louny"
    poradi: 59
    procent: "2,67"
  * nazev: "Mělník"
    poradi: 52
    procent: "8,49"
  * nazev: "Mladá Boleslav"
    poradi: 46
    procent: "11,29"
  * nazev: "Most"
    poradi: 34
    procent: "18,89"
  * nazev: "Náchod"
    poradi: 11
    procent: "39,98"
  * nazev: "Nový Jičín"
    poradi: 27
    procent: "21,79"
  * nazev: "Nymburk"
    poradi: 49
    procent: "10,28"
  * nazev: "Opava"
    poradi: 56
    procent: "4,73"
  * nazev: "Pelhřimov"
    poradi: 29
    procent: "20,79"
  * nazev: "Písek"
    poradi: 6
    procent: "49,35"
  * nazev: "Prachatice"
    poradi: 7
    procent: "48,53"
  * nazev: "Prostějov"
    poradi: 53
    procent: "7,14"
  * nazev: "Přerov"
    poradi: 50
    procent: "10,08"
  * nazev: "Příbram"
    poradi: 43
    procent: "12,69"
  * nazev: "Rakovník"
    poradi: 45
    procent: "12,2"
  * nazev: "Rokycany"
    poradi: 14
    procent: "36,23"
  * nazev: "Rychnov nad Kněžnou"
    poradi: 32
    procent: "19,23"
  * nazev: "Semily"
    poradi: 18
    procent: "27,63"
  * nazev: "Sokolov"
    poradi: 16
    procent: "35,55"
  * nazev: "Strakonice"
    poradi: 39
    procent: "15,27"
  * nazev: "Svitavy"
    poradi: 47
    procent: "10,99"
  * nazev: "Šumperk"
    poradi: 22
    procent: "24,91"
  * nazev: "Tábor"
    poradi: 17
    procent: "28,35"
  * nazev: "Tachov"
    poradi: 31
    procent: "19,29"
  * nazev: "Teplice"
    poradi: 40
    procent: "14,8"
  * nazev: "Trutnov"
    poradi: 9
    procent: "40,94"
  * nazev: "Třebíč"
    poradi: 28
    procent: "21,01"
  * nazev: "Uherské Hradiště"
    poradi: 60
    procent: "2,25"
  * nazev: "Ústí nad Orlicí"
    poradi: 15
    procent: "35,85"
  * nazev: "Vsetín"
    poradi: 3
    procent: "59,9"
  * nazev: "Vyškov"
    poradi: 24
    procent: "24,08"
  * nazev: "Znojmo"
    poradi: 36
    procent: "16,5"
  * nazev: "Žďár nad Sázavou"
    poradi: 26
    procent: "22,89"



for {nazev: mesto, poradi, procent} in data
  $container.append """
  <a class='itm' data-lightbox="image-1"  data-title='#{mesto}' href='https://samizdat.cz/data/zelen-mesta/data/okresni_img/#{mesto}.png'>
    <img src='https://samizdat.cz/data/zelen-mesta/data/okresni_img_small/#{mesto}.png' alt='#{mesto}' />
    <span class='title'>
      <b>#{mesto}</b><br><span>#{poradi}. v pořadí<br>#{procent} % zeleně</span>
    </span>
  </a>
  """
